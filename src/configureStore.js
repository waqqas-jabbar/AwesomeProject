// import middlewares
import { createStore, applyMiddleware } from 'redux';

import reducer from './reducers';

const createStoreWithMiddleware = applyMiddleware(
)(createStore);

export default function configureStore(initialState) {
  return createStoreWithMiddleware(reducer, initialState);
};
