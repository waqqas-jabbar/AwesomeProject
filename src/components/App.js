import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import Counter from '../components/Counter';
import * as actions from '../actions';
import { connect } from 'react-redux';

class App extends Component{
  render() {
    const { counter, actions } = this.props;

    return (
    <Counter
      counter={counter}
      {...actions} />
    );
  }
}

export default connect(state => ({
    counter: state.counter
  }),
  (dispatch) => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(App);
