import React, { Component } from 'react';

import {
  AppRegistry
} from 'react-native';

import {
    Provider,
} from 'react-redux';

import configureStore from './src/configureStore';

import App from './src/components/App';

class Application extends Component {
  render() {
    const store = configureStore({});

    return (
      <Provider store={store}>
          <App />
      </Provider>
    );
  }
}

AppRegistry.registerComponent('AwesomeProject', () => Application);
