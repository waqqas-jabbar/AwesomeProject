import { expect } from 'chai';

import counterReducer from "../../src/reducers/counter";
import * as types from "../../src/actions/types";

describe('Counter Reducer', () => {
  it('handles action of unknown type', () => {
    expect(counterReducer(0,{})).to.be.equal(0);
  });

  it('starts counter from zero', () => {
    expect(counterReducer(undefined,{})).to.be.equal(0);
  });

  it('increments counter on INCREMENT action', () => {
    const action = {
      type: types.INCREMENT
    }
    expect(counterReducer(0,action)).to.be.equal(1);
  });

  it('decrements counter on DECREMENT action', () => {
    const action = {
      type: types.DECREMENT
    }
    expect(counterReducer(0,action)).to.be.equal(-1);
  });

});
