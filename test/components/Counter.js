import React, { View, Text, StyleSheet } from 'react-native';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import Counter from '../../src/components/Counter';

describe('<Counter />', () => {
  let component;

  const props = {
    counter: 0,
    increment: () => {},
    decrement: () => {}
  };

  beforeEach( () => {
      component = shallow(<Counter {...props}/>);
  });

  it('should render 1 view component', () => {
     expect(component.find(View)).to.have.length(1);
  });
});
