import { expect } from 'chai';

import * as types from "../../src/actions/types";
import * as actions from "../../src/actions";


describe('actions', () => {
    describe('increment action', ()=> {
        it('returns the correct type', () => {
            const action = actions.increment();

            expect(action.type).to.be.equal(types.INCREMENT);
        });
    });

    describe('decrement action', ()=> {
        it('returns the correct type', () => {
            const action = actions.decrement();

            expect(action.type).to.be.equal(types.DECREMENT);
        });
    });

});
